# PorraMundial

## Reglas

- 5€ por cabeza
- Hay que decir: 
    - 4 finalistas (+1 punto por cada acertado)
    - Ronda en la que España se va a eliminar (+1 si aciertas)
    - Pichichi (+1 pto si aciertas)
    - Ganador Mundial (+2 pto)
- Participa el que haya pagado antes del Sab 19 Nov 20:00
- Escribidme por whatsapp y os añado

## Premios

Ganador porra: Se lleva 70% del bote, el segundo 30%. 
En caso de empate, pues que los empatados decidan si van a medias, o se echa a suertes

## Apuestas

#### Antuki
- Finalistas: Brasil, Argentina, Francia y Bélgica
- España se elimina en: Cuartos -------------- NO
- Ganador: Brasil
- Pichichi: Messi

#### Dani
- Finalistas: Brasil, Holanda, Francia, España
- España se elimina en: semis -------------- NO
- Ganador: Brasil
- Pichichi: Neymar


#### Pedris
- Finalistas: Brasil, Argentina, España y Uruguay.
- España no se elimina -------------- NO
- Ganador: España. -------------- NO
- Pichichi: Morata. -------------- NO

#### Jona
- Finalistas: Brasil, Argentina, FRANCIA, Alemania
- España se elimina en: octavos
- Ganador: Francia
- Pichichi: messi

#### Jossi
- Finalistas: Brasil Argentina España Alemania 
- España se elimina en: Semis -------------- NO
- Pichichi: Morata -------------- NO
- Ganador: Alemania

#### Javi P
- Finalistas: Brasil, Argentina, Inglaterra, Alemania
- España se elimina en: Cuartos -------------- NO
- Pichichi: Harry Kane
- Ganador: Inglaterra

#### Clau
- Finalistas: Francia, Brasil, Alemania y España.
- España de elimina en: semifinal -------------- NO
- Pichichi: Mbappe
- Ganador: Francia

#### Juan Lagares

- Finalistas: Francia, Holanda, Uruguay, Brasil
- España se elimina en: Cuartos -------------- NO
- Pichichi: Darwin Núnez
- Ganador: Uruguay

#### Ale Lagares
- Finalistas: FRANCIA - BRASIL - ARGENTINA - URUGUAY 
- España se elimina en: CUARTOS -------------- NO
- Pichichi: MBAPPE
- Ganador: BRASIL

